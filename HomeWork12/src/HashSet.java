public class HashSet<V> implements Set<V> {

    private HashMap<V, String> mapHashSet = new HashMap<>();
    @Override
    public void add(V value) {
        mapHashSet.put(value, "");
    }

    @Override
    public boolean contains(V value) {
        return mapHashSet.get(value) != null;
    }
}
