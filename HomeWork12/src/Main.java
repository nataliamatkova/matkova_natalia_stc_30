public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        map.put("Марсель", 26);
        map.put("Денис", 30);
        map.put("Илья", 28);
        map.put("Неля", 18);
        map.put("Катерина", 23);
        map.put("Полина", 20);
        map.put("Александр", 21);
        map.put("Иван", 27);
        map.put("Алексей", 27);
        map.put("Ольга", 30);;
        map.put("Игорь", 22);
        map.put("Игорь", 25);
        map.put("Игорь", 26);
        //int i =0;
        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Денис"));
        System.out.println(map.get("Илья"));
        System.out.println(map.get("Неля"));
        System.out.println(map.get("Катерина"));
        System.out.println(map.get("Полина"));
        System.out.println(map.get("Александр"));
        System.out.println(map.get("Иван"));
        System.out.println(map.get("Алексей"));
        System.out.println(map.get("Ольга"));
        System.out.println(map.get("Игорь")); //26
        System.out.println(map.get("Анна")); //null

        Set<String> set = new HashSet<>();
        set.add("Денис");
        set.add("Ольга");
        set.add("Полина");
        set.add("Илья");
        set.add("Иван");
        set.add("Иван");
        //int i = 0;
        System.out.println(set.contains("Денис"));
        System.out.println(set.contains("Ольга"));
        System.out.println(set.contains("Полина"));
        System.out.println(set.contains("Илья"));
        System.out.println(set.contains("Иван"));
        System.out.println(set.contains("Мария")); //false

    }
}
