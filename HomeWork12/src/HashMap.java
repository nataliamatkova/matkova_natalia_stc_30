public class HashMap<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;
    private MapEntry<K, V> entries[] = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next; // связный список

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) { // если в ячейке нет элементов
            entries[index] = newMapEntry; // добавляем новый элемент
        } else {
            // если в этой ячейке (bucket) уже есть элемент, запоминяем его
            MapEntry<K, V> current = entries[index];
            // теперь дойдем до последнего элемента в этом списке
            while (current.next != null) {
                current = current.next;
            }
            // теперь мы на последнем элементе
            if (newMapEntry.key.equals(current.key)) { //если ключ текущего элемента = ключу нового элемента
                current.value = newMapEntry.value; //перезаписываем значение элемента
            } else { //если ключи не равны
                current.next = newMapEntry; //просто добавляем новый элемент в конец
            }
        }
    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (entries.length - 1); //вычисляем индекс элемента
        MapEntry<K, V> current = entries[index]; //находим в массиве ячейку под этим индексом
        if (current != null) { //если ячейка пустая, возвращаем null, если не пустая:
            if (current.key.equals(key)) { //сравниваем искомый ключ с ключом элемента в ячейке
                return current.value; //если ключи равны, возвращаем значение
            } else { //если ключи не равны
                while (current.next != null) { // проходим по связному списку до конца
                    current = current.next;
                    if (current.key.equals(key)) { //и сравниваем ключ с элементами в связном списке, если они равны
                        return current.value; // возвращаем значение
                    }
                    return null;
                }
            }
        }
        return null;
    }
}

