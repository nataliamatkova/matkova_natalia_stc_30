import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a positive number");
        int number = scanner.nextInt();
        fib(number, 0, 1, 2);
    }

    /*  lastNumber - номер последнего вычисленного числа Фибоначчи
        previous, previous1   - значения предыдущих вычисленных чисел
        sum - сумма предыдущих вычисленных значений */

    public static void fib(int number, long previous, long previous1, int lastNumber) {
        if (number == 0) {
            System.out.println(0);
        }
        if (number == 1) {
            System.out.println(1);
        }
        if (number >= 2) {
            if (lastNumber <= number) {
                long sum = previous + previous1;
                System.out.println(sum);
                fib(number, previous1, sum, ++lastNumber);
            }
        }
    }
}
