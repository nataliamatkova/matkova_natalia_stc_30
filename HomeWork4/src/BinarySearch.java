public class BinarySearch {

    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 100}; //отсортированный массив
        System.out.println("Index of element: " + binarySearch(array, 45, 0, array.length - 1));
    }

    public static int binarySearch(int[] array, int element, int left, int right) {
        if (left != right) {
            int middle = (left + right) / 2;
            if (array[middle] > element) {
                return binarySearch(array, element, left, middle - 1);
            } else if (array[middle] < element) {
                return binarySearch(array, element, middle + 1, right);
            } else {
                return middle;
            }
        }
        return -1; //элемента нет в массиве
    }
}