public class Main {
    public static void main(String[] args) {

        //ArrayList
        List<Integer> arrayList = new ArrayList<>();

        for (int i = 0; i < 17; i++) {
            arrayList.add(i);
        }

        System.out.println("Size of array list: " + arrayList.size());
        System.out.println(arrayList.contains(7));
        arrayList.removeByIndex(9);
        arrayList.removeByIndex(-4);
        arrayList.insert(33, 5);
        System.out.println("Size of array list: " + arrayList.size());

        Iterator<Integer> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        arrayList.reverse();
        Iterator<Integer> iteratorReverse = arrayList.iterator();
        while (iteratorReverse.hasNext()) {
            System.out.println(iteratorReverse.next());
        }


        //ArrayList String пример со строками
        List<String> arrayListString = new ArrayList<>();

        arrayListString.add("hello");
        arrayListString.add("bye");
        arrayListString.add("good morning");

        System.out.println("Size of array list of strings: " + arrayListString.size());
        System.out.println(arrayListString.contains("bye"));
        System.out.println(arrayListString.get(0));
        arrayListString.removeByIndex(2);
        arrayListString.insert("hi", 1);

        Iterator<String> iteratorString = arrayListString.iterator();
        while (iteratorString.hasNext()) {
            System.out.println(iteratorString.next());
        }


        //LinkedList
        List<Integer> linkedList = new LinkedList<>();

        linkedList.add(9);
        linkedList.add(778);
        linkedList.add(23);
        linkedList.add(11);
        linkedList.add(56);
        linkedList.add(4);

        System.out.println("Size of linked list: " + linkedList.size());
        System.out.println(linkedList.get(1));
        linkedList.insert(19, 2);
        linkedList.insert(199, 12);
        System.out.println(linkedList.contains(56));
        System.out.println(linkedList.contains(100));
        linkedList.removeByIndex(4);
        linkedList.removeFirst(9);

        Iterator<Integer> linkedListIterator = linkedList.iterator();
        while (linkedListIterator.hasNext()) {
            System.out.println(linkedListIterator.next());
        }

        linkedList.reverse();

        linkedListIterator = linkedList.iterator();
        while (linkedListIterator.hasNext()) {
            System.out.println(linkedListIterator.next());
        }
    }
}
