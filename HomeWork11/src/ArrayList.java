//список на основе массива
public class ArrayList<T> implements List<T> {
    //константа для начального размера массива
    private static final int DEFAULT_SIZE = 10;
    //поле, представляющее собой массив, в котором храним элементы
    private T[] data;
    //количество элементов в массиве (count != length)
    private int count;

    public ArrayList() {
        this.data = (T[]) new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator<T> {
        private int current = 0;

        @Override
        public T next() {
            T value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }


    @Override
    public T get(int index) {
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");
        return null;
    }

    @Override
    public int indexOf(T element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(T element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(T element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1); // побитовый сдвиг вправо. oldLength >> 1 = oldLength/2
        T[] newData = (T[]) new Object[newLength];
        System.arraycopy(this.data, 0, newData, 0, oldLength);
        this.data = newData;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(T element) {
        int indexOfRemovingElement = indexOf(element);
        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        count--;
    }

    @Override
    public void removeByIndex(int index) {
        if (index < count && index >= 0) {
            for (int i = index; i < count - 1; i++) { //проходим по массиву от index-а до конца
                this.data[i] = this.data[i + 1]; //и заменяем значения элементов на следующий
            }
            count--; //уменьшаем количество элементов в массиве
        } else {
            System.out.println("Такого индекса нет");
        }
    }

    @Override
    public void insert(T element, int index) {
        if (index <= count && index >= 0) {
            if (count == this.data.length - 1) {
                resize();
            } else {
                count++; //увеличиваем количество элементов в массиве
                for (int i = count - 1; i > index; i--) { //проходим массив в обратном порядке до index
                    this.data[i] = this.data[i - 1]; //и заменяем значения элементов на предыдущий
                }
                this.data[index] = element;
            }
        } else {
            System.out.println("Вышли за пределы массива");
        }
    }

    @Override
    public void reverse() {
        T reverseValue;
        for (int i = 0; i < count / 2; i++) { // доходим до середины массива
            reverseValue = data[i]; // сохраняем значение первого элемента (i=0)
            data[i] = data[count - 1 - i]; // теперь первый элемент = последнему
            data[count - 1 - i] = reverseValue; // а последний равен первому
        }
    }
}
