public class LinkedList<E> implements List<E> {

    private class LinkedListIterator implements Iterator<E> {

        private Node<E> element = first; // первый элемент
        private int elementCount = 0; //количество элементов

        @Override
        public E next() {
            E value = element.value; //значение элемента
            element = element.next; //ссылка на следующий элемент
            elementCount++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return elementCount < count;
        }
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator();
    }

    private Node<E> first;
    private Node<E> last;
    private int count;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public E get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node<E> current = this.first;
            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Такого элемента нет");
        return null;
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        Node<E> current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void add(E element) {
        Node<E> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
            // новый узел теперь последний
            last = newNode;
        }
        count++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(E element) {
        if (this.contains(element)) {
            Node<E> current = this.first;
            this.first = current.next;
            count--;
        } else {
            System.out.println("Такого элемента нет");
        }
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element) != -1;
    }

    @Override
    public void insert(E element, int index) {
        Node<E> newNode = new Node<>(element); // новый узел
        Node<E> current = this.first; // текущий узел
        Node<E> nextCurrent = current.next; // следующий после текущего
        if (index > 0 && index < count) {
            int i = 0;
            while (i < index - 1) { // проходим по всему списку до нужного индекса
                current = current.next;
                nextCurrent = current.next;
                i++;
            }
            current.next = newNode; // создаем связи для нового элемента
            newNode.next = nextCurrent;
            count++;
        } else if (index == 0) {
            newNode.next = this.first;
            this.first = newNode;
            count++;
        } else {
            System.out.println("Такого индекса нет");
        }
    }

    @Override
    public void removeByIndex(int index) {
        Node<E> current = this.first; //текущий узел (index = 0)
        Node<E> nextCurrent = current.next; // следующий после текущего (index = 1)
        if (index > 0 && index < count) {
            int i = 0;
            while (i < index - 1) { // проходим по всему списку до нужного индекса
                current = current.next; // текущему узлу присваивается значение следующего
                nextCurrent = current.next; // следущему узлу - следующий после текущего
                i++;
            }
            current.next = nextCurrent.next; //присваивается значение через элемент (пропускаем элемент, который нужно удалить)
            count--; // уменьшаем количество узлов
        } else if (index == 0) {
            //this.removeFirst(index);
            //Node<E> current = this.first;
            this.first = current.next;
            count--;
        } else {
            System.out.println("Такого индекса нет");
        }
    }

    @Override
    public void reverse() {
        Node<E> current = this.first; // текущий узел, ссылается на первый элемент
        Node<E> previous = null; // предыдущий узел, ссылка null
        while (current != null) { // проходим по списку
            Node<E> next = current.next;  // следующий узел, ссылается на следующиий за текущим
            current.next = previous; // "разворачиваем" ссылку в обратную сторону, теперь следующий узел ссылается на предыдущий
            previous = current; // идем дальше по списку: от предыдущего узла к текущему
            current = next; // от текущему к следующему
        }
        this.first = previous;
    }
}
