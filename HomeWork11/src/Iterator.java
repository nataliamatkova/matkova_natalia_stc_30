public interface Iterator<A> {
    // возвращает следующий элемент
    A next ();
    //проверяет, есть ли следующий эелемент
    boolean hasNext();
}
