package ru.inno.tree;

import java.util.Deque;
import java.util.LinkedList;

public class BinarySearchTreeImpl<T extends Comparable<T>> implements BinarySearchTree<T> {

    static class Node<E> {
        E value;
        Node<E> left;
        Node<E> right;

        public Node(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    private Node<T> root;

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    private Node<T> insert(Node<T> root, T value) {
        if (root == null) {
            root = new Node<>(value);
        }
        // значение меньше корня
        else if (value.compareTo(root.value) < 0) {
            // добавляем в левое поддерево
            root.left = insert(root.left, value);
        } else {
            root.right = insert(root.right, value);
        }
        return root;
    }

    @Override
    public void printDfsByQueue() {
        dfs(this.root);
    }

    private void dfs(Node<T> root) {
        if (root != null) {
            dfs(root.left);
            System.out.println(root.value + " ");
            dfs(root.right);
        }
    }


    @Override
    public void printDfsByStack() {
        Deque<Node<T>> stack = new LinkedList<>();
        stack.push(root);
        Node<T> current;
        while (!stack.isEmpty()) {
            current = stack.pop();
            System.out.println(current.value + " ");
            if (current.left != null) {
                stack.push(current.left);
            }
            if (current.right != null) {
                stack.push(current.right);
            }
        }
    }

    @Override
    public void printBfs() {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);
        Node<T> current;
        while (!queue.isEmpty()) {
            current = queue.poll();
            System.out.println(current.value + " ");
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }
    }


    @Override
    public void printBfsByLevels() {
        bfsByLevels(this.root);
    }

    private void bfsByLevels(Node<T> root) {
        LinkedList<Node<T>> currentLevel = new LinkedList<>();
        LinkedList<Node<T>> nextLevel = currentLevel;

        StringBuilder stringBuilder = new StringBuilder();
        currentLevel.add(root);
        stringBuilder.append(root.value + "\n");

        while (nextLevel.size() > 0) {
            nextLevel = new LinkedList<>();
            for (int i = 0; i < currentLevel.size(); i++) {
                Node<T> current = currentLevel.get(i);
                if (current.left != null) {
                    nextLevel.add(current.left);
                    stringBuilder.append(current.left.value + " ");
                }
                if (current.right != null) {
                    nextLevel.add(current.right);
                    stringBuilder.append(current.right.value + " ");
                }
            }
            if (nextLevel.size() > 0) {
                stringBuilder.append("\n");
                currentLevel = nextLevel;
            }
        }
        System.out.println(stringBuilder.toString());
    }


    @Override
    public void remove(T value) {
        this.root = removeRecursive(this.root, value);
    }

    private Node<T> removeRecursive(Node<T> current, T value) {
        if (current == null) {
            return null;
        }
        // узел найден
        if (value == current.value) {

            // 1.вариант: у узла нет потомков
            if (current.left == null && current.right == null) {
                return null;
            }

            // 2.вариант: у узла есть один потомок - слева или справа
            if (current.right == null) {
                return current.left;
            }
            if (current.left == null) {
                return current.right;
            }

            // 3.вариант: у узла 2 потомка
            Node<T> smallestValue = findSmallestValue(current.right);
            current.value = smallestValue.value;
            current.right = removeRecursive(current.right, smallestValue.value);
            return current;
        }
        // поиск нужного узла по левому поддереву
        if (value.compareTo(current.value) < 0) {
            current.left = removeRecursive(current.left, value);
            return current;
        }
        // поиск нужного узла по правому поддереву
        current.right = removeRecursive(current.right, value);
        return current;
    }

    // вспогательная функция: поиск узла, который заменит удаленный узел
    private Node<T> findSmallestValue(Node<T> root) {
        if (root.left == null) {
            return root;
        } else {
            findSmallestValue(root.left);
        }
        return root;
    }

    @Override
    public boolean contains(T value) {
        Deque<Node<T>> queue = new LinkedList<>();
        queue.add(root);
        Node<T> current;
        while (!queue.isEmpty()) {
            current = queue.poll();
            if (current.value == value) {
                return true;
            }
            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }
        return false;
    }
}

