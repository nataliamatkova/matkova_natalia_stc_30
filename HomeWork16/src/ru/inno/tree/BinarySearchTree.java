package ru.inno.tree;

public interface BinarySearchTree<T extends Comparable<T>> {
    void insert(T value);
    void printDfsByQueue();
    void printDfsByStack();
    void printBfs();
    void printBfsByLevels();
    void remove(T value);
    boolean contains(T value);
}