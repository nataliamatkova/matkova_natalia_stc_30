package ru.inno;

import ru.inno.graph.*;
import ru.inno.tree.*;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree<Integer> tree = new BinarySearchTreeImpl<>();
        tree.insert(8);
        tree.insert(3);
        tree.insert(10);
        tree.insert(1);
        tree.insert(6);
        tree.insert(14);
        tree.insert(4);
        tree.insert(7);
        tree.insert(13);

        tree.printBfsByLevels();
        System.out.println(tree.contains(3));
        System.out.println(tree.contains(11));
        tree.remove(6);
        tree.printBfsByLevels();
        //tree.printDfsByQueue();
        //tree.printDfsByStack();
        //tree.printBfs();


        OrientedGraph orientedGraph = new AdjacencyListImpl(7);

        Vertex vertex0 = new VertexIntegerImpl(0);
        Vertex vertex1 = new VertexIntegerImpl(1);
        Vertex vertex2 = new VertexIntegerImpl(2);
        Vertex vertex3 = new VertexIntegerImpl(3);
        Vertex vertex4 = new VertexIntegerImpl(4);
        Vertex vertex5 = new VertexIntegerImpl(5);
        Vertex vertex6 = new VertexIntegerImpl(6);

        Edge edge0 = new EdgeTwoVerticesImpl(vertex0, vertex1, 1);
        Edge edge1 = new EdgeTwoVerticesImpl(vertex0, vertex2, 1);
        Edge edge2 = new EdgeTwoVerticesImpl(vertex1, vertex3, 1);
        Edge edge3 = new EdgeTwoVerticesImpl(vertex2, vertex3, 1);
        Edge edge4 = new EdgeTwoVerticesImpl(vertex2, vertex4, 1);
        Edge edge5 = new EdgeTwoVerticesImpl(vertex2, vertex5, 1);
        Edge edge6 = new EdgeTwoVerticesImpl(vertex5, vertex4, 1);
        Edge edge7 = new EdgeTwoVerticesImpl(vertex5, vertex6, 1);
        Edge edge8 = new EdgeTwoVerticesImpl(vertex3, vertex6, 1);

        orientedGraph.addEdge(edge0);
        orientedGraph.addEdge(edge1);
        orientedGraph.addEdge(edge2);
        orientedGraph.addEdge(edge3);
        orientedGraph.addEdge(edge4);
        orientedGraph.addEdge(edge5);
        orientedGraph.addEdge(edge6);
        orientedGraph.addEdge(edge7);
        orientedGraph.addEdge(edge8);

        orientedGraph.printAdjacencyList();
        System.out.println("Breadth first search:");
        orientedGraph.printBfs(0);
        orientedGraph.printBfs(1);
        orientedGraph.printBfs(2);
        orientedGraph.printBfs(3);
        orientedGraph.printBfs(4);
        orientedGraph.printBfs(5);
        orientedGraph.printBfs(6);
        System.out.println("Depth first search:");
        orientedGraph.printDfs(0);
        orientedGraph.printDfs(1);
        orientedGraph.printDfs(2);
        orientedGraph.printDfs(3);
        orientedGraph.printDfs(4);
        orientedGraph.printDfs(5);
        orientedGraph.printDfs(6);
    }
}
