package ru.inno.graph;

public interface Edge {
    Vertex getFirst();
    Vertex getSecond();
    int weight();
}
