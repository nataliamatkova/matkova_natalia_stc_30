package ru.inno.graph;

import java.util.Iterator;
import java.util.LinkedList;

public class AdjacencyListImpl implements OrientedGraph {
    private LinkedList<Integer>[] adjacencyList; //список смежности
    private int countVertices; //количество вершин

    public AdjacencyListImpl(int countVertices) {
        this.countVertices = countVertices;
        adjacencyList = new LinkedList[countVertices];
        for (int i = 0; i < countVertices; i++) {
            adjacencyList[i] = new LinkedList<>();
        }
    }

    @Override
    public void addEdge(Edge edge) {
        adjacencyList[edge.getFirst().getNumber()].add(edge.getSecond().getNumber());
    }

    @Override
    public void printAdjacencyList() {
        for (int i = 0; i < countVertices; i++) {
            if (adjacencyList[i].size() > 0) {
                System.out.print("Vertex " + i + " is connected to: ");
                for (int j = 0; j < adjacencyList[i].size(); j++) {
                    System.out.print(adjacencyList[i].get(j) + " ");
                }
                System.out.println();
            }
        }
    }

    @Override
    public void printBfs(int vertex) {
        // массив с посещенными вершинами, по умолчанию все вершины - не посещены
        boolean[] visited = new boolean[countVertices];
        // создаем очередь для обхода bfs
        LinkedList<Integer> queue = new LinkedList<>();
        // отмечаем текущую вершину, как посещенную
        visited[vertex] = true;
        queue.add(vertex);
        while (queue.size() != 0) {
            // выводим вершину из очереди и распечатываем ее
            vertex = queue.poll();
            System.out.print(vertex + " ");
            // проходим по списку смежности
            Iterator<Integer> iterator = adjacencyList[vertex].listIterator();
            while (iterator.hasNext()) {
                int neighborVertex = iterator.next();
                // если соседняя вершина еще не была посещена,
                // то помечаем её как посещенную и ставим в очередь
                if (!visited[neighborVertex]) {
                    visited[neighborVertex] = true;
                    queue.add(neighborVertex);
                }
            }
        }
        System.out.println();
    }


    @Override
    public void printDfs(int vertex) {
        // массив с посещенными вершинами, по умолчанию все вершины - не посещены
        boolean[] visited = new boolean[countVertices];
        // вызов вспомогательной рекурсивной функции для обхода dfs
        dfsRecursive(vertex, visited);
        System.out.println();
    }

    private void dfsRecursive(int vertex, boolean[] visited) {
        // отмечаем текущую вершину, как посещенную и распечатываем ее
        visited[vertex] = true;
        System.out.print(vertex + " ");
        // рекурсия для всех вершин, смежных с текущей вершиной
        Iterator<Integer> iterator = adjacencyList[vertex].listIterator();
        while (iterator.hasNext()) {
            int neighborVertex = iterator.next();
            if (!visited[neighborVertex])
                dfsRecursive(neighborVertex, visited);
        }
    }

}
