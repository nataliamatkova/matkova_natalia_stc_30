package ru.inno.graph;

public class VertexIntegerImpl implements Vertex {

    private int number;

    public VertexIntegerImpl(int number) {
        this.number = number;
    }

    @Override
    public int getNumber() {
        return number;
    }
}
