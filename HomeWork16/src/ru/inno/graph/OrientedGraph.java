package ru.inno.graph;

public interface OrientedGraph {

    void addEdge(Edge edge);
    void printAdjacencyList();
    void printBfs(int vertex);
    void printDfs(int vertex);
}
