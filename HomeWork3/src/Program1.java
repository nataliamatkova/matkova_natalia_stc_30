import java.util.Arrays;
import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter array size and elements");
        int size = scanner.nextInt();
        int[] array = new int[size];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println("Source array: " + Arrays.toString(array)); //исходный массив

        System.out.println("Sum = " + arraySum(array)); //функция выводит сумму элементов массива
        oppositeArray(array);   //разворот массива
        System.out.println("Average = " + arraySum(array) / array.length); //среднеарифметическое
        minMax(array);             //меняет местами макс. и мин. элементы массива
        bubbleSort(array);        //сортировка пузырьком
        arrayToNumber(array);     //преобразует массив в число
    }

    public static int arraySum(int[] array) { //функция подсчитывает сумму элементов массива
        int result = 0;
        for (int i = 0; i < array.length; i++) {
            result = result + array[i];
        }
        return result;
    }

    public static void oppositeArray(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int opposite;
            opposite = array[array.length - i - 1];
            array[array.length - i - 1] = array[i]; //замена элемента в массиве противоположным
            array[i] = opposite;
        }
        System.out.println("Opposite array: " + Arrays.toString(array));
    }


    public static void minMax(int[] array) { //меняет местами макс. и мин. элементы массива
        int indexOfMin = 0;
        int indexOfMax = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[indexOfMin] > array[i]) {
                indexOfMin = i;
            } else if (array[indexOfMax] < array[i]) {
                indexOfMax = i;
            }
        }
        int temp = array[indexOfMin]; //временная переменная
        array[indexOfMin] = array[indexOfMax];
        array[indexOfMax] = temp;
        System.out.println("Swap min and max: " + Arrays.toString(array));
    }

    public static void bubbleSort(int[] array) {         //сортировка пузырьком
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {  //перебор значений
                if (array[j] > array[j + 1]) { //сравнение соседних элементов
                    int temp1 = array[j]; // временная переменная для замены значений
                    array[j] = array[j + 1];
                    array[j + 1] = temp1;
                }
            }
        }
        System.out.println("Sorted array: " + Arrays.toString(array));
    }

    public static void arrayToNumber(int[] array) { //преобразование массива в число
        int number = 0;
        for (int i = 0; i < array.length; i++) {
            number = number * 10 + array[i];
        }
        System.out.println(number);
    }
}