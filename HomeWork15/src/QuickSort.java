import java.util.List;

public class QuickSort {

    public static List<String> quickSort(List<String> list, int min, int max) {
        if (list.size() == 0) // выход из рекурсии, если лист пустой
            return null;
        int middle = (min + max) / 2; // выбираем середину - опорную точку
        String middleElement = list.get(middle);
        int i = min; // нулевой элемент списка
        int j = max; // последний элемент списка
        while (i <= j) { // относительно опорной точки определяем меньшие элементы слева
            while (list.get(i).compareTo(middleElement) < 0) {
                i++;
            }
            while (list.get(j).compareTo(middleElement) > 0) { //большие справа
                j--;
            }
            if (i <= j) { //меняем местами
                String temp = list.get(i);
                list.set(i, list.get(j));
                list.set(j, temp);
                i++;
                j--;
            }
        }
        if (min < j) // запускаем рекурсию с элементами меньшими, чем middleElement
            quickSort(list, min, j);
        if (max > i) // запускаем рекурсию с элементами большими, чем middleElement
            quickSort(list, i, max);
        return list;
    }
}
