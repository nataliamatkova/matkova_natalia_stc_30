import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<String> wordList = new ArrayList<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("text.txt"));
            String current = bufferedReader.readLine();
            while (current != null) {
                //разбиваем текст на массив строк, удаляя знаки препинания
                String[] data = current.replaceAll("[!?;:.,]", "").split(" ");
                //массив помещаем в лист
                wordList.addAll(Arrays.asList(data));
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        QuickSort.quickSort(wordList, 0, wordList.size() - 1);
        System.out.println(wordList);
    }
}
