package models;

import java.util.Objects;

public class Lessons {
    private Long lessonId;
    private String lessonName;
    private String lessonDate;
    private Courses course;

    public Lessons() {
    }

    public Lessons(String lessonName, String lessonDate) {
        this.lessonName = lessonName;
        this.lessonDate = lessonDate;
    }

    public Lessons(Long lessonId, String lessonName, String lessonDate, Courses course) {
        this.lessonId = lessonId;
        this.lessonName = lessonName;
        this.lessonDate = lessonDate;
        this.course = course;
    }

    public Lessons(Long lessonId, String lessonName, String lessonDate) {
        this.lessonId = lessonId;
        this.lessonName = lessonName;
        this.lessonDate = lessonDate;
    }

    public Long getLessonId() {
        return lessonId;
    }

    public void setLessonId(Long lessonId) {
        this.lessonId = lessonId;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public Courses getCourse() {
        return course;
    }

    public void setCourse(Courses course) {
        this.course = course;
    }

    public String getLessonDate() {
        return lessonDate;
    }

    public void setLessonDate(String lessonDate) {
        this.lessonDate = lessonDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Lessons lessons = (Lessons) o;
        return lessonId == lessons.lessonId &&
                Objects.equals(lessonName, lessons.lessonName) &&
                Objects.equals(course, lessons.course) &&
                Objects.equals(lessonDate, lessons.lessonDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lessonId, lessonName, course, lessonDate);
    }

    @Override
    public String toString() {
        return "Lessons{" +
                "lessonId=" + lessonId +
                ", lessonName='" + lessonName + '\'' +
                ", course=" + course +
                ", lessonDate='" + lessonDate + '\'' +
                '}';
    }
}
