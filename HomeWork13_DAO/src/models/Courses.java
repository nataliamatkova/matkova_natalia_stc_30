package models;

import java.util.List;
import java.util.Objects;

public class Courses {
    private Long courseId;
    private String courseName;
    private String startDateCourse;
    private String endDateCourse;
    private List<Teachers> teachersList;
    private List<Lessons> lessonsList;

    public Courses() {
    }

    public Courses(String courseName, String startDateCourse, String endDateCourse) {
        this.courseName = courseName;
        this.startDateCourse = startDateCourse;
        this.endDateCourse = endDateCourse;
    }

    public Courses(Long courseId, String courseName, String startDateCourse, String endDateCourse,
                   List<Teachers> teachersList, List<Lessons> lessonsList) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.startDateCourse = startDateCourse;
        this.endDateCourse = endDateCourse;
        this.teachersList = teachersList;
        this.lessonsList = lessonsList;
    }

    public Courses(Long courseId, String courseName, String startDateCourse, String endDateCourse) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.startDateCourse = startDateCourse;
        this.endDateCourse = endDateCourse;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getStartDateCourse() {
        return startDateCourse;
    }

    public void setStartDateCourse(String startDateCourse) {
        this.startDateCourse = startDateCourse;
    }

    public String getEndDateCourse() {
        return endDateCourse;
    }

    public void setEndDateCourse(String endDateCourse) {
        this.endDateCourse = endDateCourse;
    }

    public List<Teachers> getTeachersList() {
        return teachersList;
    }

    public void setTeachersList(List<Teachers> teachersList) {
        this.teachersList = teachersList;
    }

    public List<Lessons> getLessonsList() {
        return lessonsList;
    }

    public void setLessonsList(List<Lessons> lessonsList) {
        this.lessonsList = lessonsList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Courses courses = (Courses) o;
        return courseId == courses.courseId &&
                Objects.equals(courseName, courses.courseName) &&
                Objects.equals(startDateCourse, courses.startDateCourse) &&
                Objects.equals(endDateCourse, courses.endDateCourse) &&
                Objects.equals(teachersList, courses.teachersList) &&
                Objects.equals(lessonsList, courses.lessonsList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(courseId, courseName, startDateCourse, endDateCourse, teachersList, lessonsList);
    }

    @Override
    public String toString() {
        return "Courses{" +
                "courseId=" + courseId +
                ", courseName='" + courseName + '\'' +
                ", startDateCourse='" + startDateCourse + '\'' +
                ", endDateCourse='" + endDateCourse + '\'' +
                ", teachersList=" + teachersList +
                ", lessonsList=" + lessonsList +
                '}';
    }
}
