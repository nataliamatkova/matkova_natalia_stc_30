package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Teachers {
    private Long teacherId;
    private String firstName;
    private String lastName;
    private Double experience;
    private List<Courses> coursesList = new ArrayList<>();

    public Teachers() {
    }

    public Teachers(String firstName, String lastName, Double experience) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
    }

    public Teachers(Long teacherId, String firstName, String lastName, Double experience,
                    List<Courses> coursesList) {
        this.teacherId = teacherId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
        this.coursesList = coursesList;
    }

    public Teachers(Long teacherId, String firstName, String lastName, Double experience) {
        this.teacherId = teacherId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getExperience() {
        return experience;
    }

    public void setExperience(Double experience) {
        this.experience = experience;
    }

    public List<Courses> getCoursesList() {
        return coursesList;
    }

    public void setCoursesList(List<Courses> coursesList) {
        this.coursesList = coursesList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teachers teachers = (Teachers) o;
        return teacherId == teachers.teacherId &&
                Objects.equals(firstName, teachers.firstName) &&
                Objects.equals(lastName, teachers.lastName) &&
                Objects.equals(experience, teachers.experience) &&
                Objects.equals(coursesList, teachers.coursesList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teacherId, firstName, lastName, experience, coursesList);
    }

    @Override
    public String toString() {
        return "Teachers{" +
                "teacherId=" + teacherId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", experience=" + experience +
                ", coursesList=" + coursesList +
                '}';
    }
}
