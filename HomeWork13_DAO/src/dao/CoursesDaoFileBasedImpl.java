package dao;

import models.Courses;
import utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CoursesDaoFileBasedImpl implements CoursesDao{

    private String fileName;
    private IdGenerator idGenerator;

    public CoursesDaoFileBasedImpl(String fileName,IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Courses, String> courseToStringMapper = course ->
            course.getCourseId() + " " +
                    course.getCourseName() + " " +
                    course.getStartDateCourse() + " " +
                    course.getEndDateCourse() + " " + "\r\n";

    private Mapper<String, Courses> stringToCourseMapper = string -> {
        String[] data = string.split(" ");
        return new Courses(Long.parseLong(data[0]), data[1], data[2], data[3]);
    };

    @Override
    public Optional<Courses> findById(Long id) {
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null){
                String[] data = current.split(" ");
                int existedId = Integer.parseInt(data[0]);
                if (existedId == id){
                    Courses courses = stringToCourseMapper.map(current);
                    return Optional.of(courses);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Courses courses) {
        try {
            courses.setCourseId(idGenerator.nextId());
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(courseToStringMapper.map(courses).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public List<Courses> findAll() {
        try {
            List<Courses> courses = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Courses course = stringToCourseMapper.map(current);
                courses.add(course);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return courses;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Courses> findByCourseName(String courseName) {
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null){
                String[] data = current.split(" ");
                String existedName = String.valueOf(data[1]);
                if (existedName.equals(courseName)){
                    Courses courses = stringToCourseMapper.map(current);
                    return Optional.of(courses);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
