package dao;

import models.Teachers;
import utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TeachersDaoFileBasedImpl implements TeachersDao{

    private String fileName;
    private IdGenerator idGenerator;

    public TeachersDaoFileBasedImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Teachers, String> teachersToStringMapper = teacher ->
            teacher.getTeacherId() + " " +
                    teacher.getFirstName() + " " +
                    teacher.getLastName() + " " +
                    teacher.getExperience() + " " +
                    teacher.getCoursesList() + " " + "\r\n";

    private Mapper<String, Teachers> stringToTeachersMapper = string -> {
        String[] data = string.split(" ");
        return new Teachers(Long.parseLong(data[0]), data[1], data[2], Double.parseDouble(data[3]));
    };

    @Override
    public Optional<Teachers> findById(Long id) {
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null){
                String[] data = current.split(" ");
                int existedId = Integer.parseInt(data[0]);
                if (existedId == id){
                    Teachers teachers = stringToTeachersMapper.map(current);
                    return Optional.of(teachers);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Teachers teachers) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(teachersToStringMapper.map(teachers).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<Teachers> findAll() {
        try {
            List<Teachers> teachers = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Teachers teacher = stringToTeachersMapper.map(current);
                teachers.add(teacher);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return teachers;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Teachers> findByLastName(String lastName) {
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null){
                String[] data = current.split(" ");
                String existedLastName = String.valueOf(data[2]);
                if (existedLastName.equals(lastName)){
                    Teachers teachers = stringToTeachersMapper.map(current);
                    return Optional.of(teachers);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
