package dao;

import models.Lessons;
import utils.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonsDaoFileBasedImpl implements LessonsDao{

    private String fileName;
    private IdGenerator idGenerator;

    public LessonsDaoFileBasedImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    private Mapper<Lessons, String> lessonsToStringMapper = lesson ->
            lesson.getLessonId() + " " +
                    lesson.getLessonName() + " " +
                    lesson.getLessonDate() + " " + "\r\n";

    private Mapper<String, Lessons> stringToLessonsMapper = string -> {
        String[] data = string.split(" ");
        return new Lessons(Long.parseLong(data[0]), data[1], data[2]);
    };


    @Override
    public Optional<Lessons> findById(Long id) {
        try{
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null){
                String[] data = current.split(" ");
                int existedId = Integer.parseInt(data[0]);
                if (existedId == id){
                    Lessons lessons = stringToLessonsMapper.map(current);
                    return Optional.of(lessons);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Lessons lessons) {
        try {
            lessons.setLessonId(idGenerator.nextId());
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(lessonsToStringMapper.map(lessons).getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public List<Lessons> findAll() {
        try {
            List<Lessons> lessons = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Lessons lesson = stringToLessonsMapper.map(current);
                lessons.add(lesson);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return lessons;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
