package dao;

import models.Teachers;

import java.util.Optional;

public interface TeachersDao extends CrudDao<Teachers>{
    Optional<Teachers> findByLastName(String lastName);

}
