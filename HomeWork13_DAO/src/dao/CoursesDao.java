package dao;

import models.Courses;

import java.util.Optional;

public interface CoursesDao extends CrudDao<Courses>{
    Optional<Courses> findByCourseName( String courseName);
}
