package dao;

public interface Mapper<X, Y> {
    Y map(X x);
}
