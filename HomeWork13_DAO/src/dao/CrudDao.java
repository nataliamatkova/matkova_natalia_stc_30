package dao;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {
    Optional<T> findById(Long id);
    void save (T entity);
    List<T> findAll();
}
