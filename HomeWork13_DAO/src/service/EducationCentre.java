package service;

import models.Courses;
import models.Lessons;
import models.Teachers;

import java.util.List;

public interface EducationCentre {
    void createSchedule(Courses courses, List<Teachers> teachersList, List<Lessons> lessonsList);
}
