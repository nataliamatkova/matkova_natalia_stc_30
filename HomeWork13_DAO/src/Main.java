import dao.*;
import models.Courses;
import models.Lessons;
import models.Teachers;
import utils.IdGeneratorImpl;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        CoursesDao coursesDao = new CoursesDaoFileBasedImpl("courses.txt", new IdGeneratorImpl("courses_sequence.txt"));
        Courses course1 = new Courses("Java", "01/11/20", "31/01/21");
        coursesDao.save(course1);



        /*TeachersDao teachersDao = new TeachersDaoFileBasedImpl("teachers.txt", new IdGeneratorImpl("teachers_sequence.txt"));
        Teachers teacher1 = new Teachers("Oleg", "Orlov", 4.5);
        teachersDao.save(teacher1);

        LessonsDao lessonsDao = new LessonsDaoFileBasedImpl("lessons.txt", new IdGeneratorImpl("lessons_sequence.txt"));
        Lessons lesson1 = new Lessons("database", "02/11/2020");
        lessonsDao.save(lesson1);*/

        /* Запись в файлы из консоли
        Scanner scanner = new Scanner(System.in);

        CoursesDao coursesDao = new CoursesDaoFileBasedImpl("courses.txt");
        int courseId = Integer.parseInt(scanner.nextLine());
        String courseName = scanner.nextLine();
        String startDateCourse = scanner.nextLine();
        String endDateCourse = scanner.nextLine();
        Courses course = new Courses(courseId, courseName, startDateCourse, endDateCourse);
        coursesDao.save(course);

        TeachersDao teachersDao = new TeachersDaoFileBasedImpl("teachers.txt");
        int teacherId = Integer.parseInt(scanner.nextLine());
        String firstName = scanner.nextLine();
        String lastName = scanner.nextLine();
        Double experience = Double.parseDouble(scanner.nextLine());
        Teachers teacher = new Teachers(teacherId, firstName, lastName, experience);
        teachersDao.save(teacher);

        LessonsDao lessonsDao = new LessonsDaoFileBasedImpl("lessons.txt");
        int lessonId = Integer.parseInt(scanner.nextLine());
        String lessonName = scanner.nextLine();
        String lessonDate = scanner.nextLine();
        Lessons lesson = new Lessons(lessonId, lessonName, lessonDate);
        lessonsDao.save(lesson);*/


        /*//Find
        coursesDao.findById(102L).ifPresent(courses -> {
            System.out.println(courses.getCourseName() + " " + courses.getStartDateCourse() + " " +
                    courses.getEndDateCourse());
        });
        System.out.println(coursesDao.findAll());
        coursesDao.findByCourseName("STC-30").ifPresent(courses -> {
            System.out.println(courses.getCourseName() + " " + courses.getStartDateCourse() + " " +
                    courses.getEndDateCourse());
        });

        System.out.println(lessonsDao.findAll());

        teachersDao.findById(203L).ifPresent(teachers -> {
            System.out.println(teachers.getFirstName() + " " + teachers.getLastName() + " " +
                    teachers.getExperience());
        });
        System.out.println(teachersDao.findAll());
        teachersDao.findByLastName("Orlov").ifPresent(teachers -> {
            System.out.println(teachers.getFirstName() + " " + teachers.getLastName() + " " +
                    teachers.getExperience());
        });*/
    }
}
