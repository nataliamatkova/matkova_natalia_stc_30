import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Random random = new Random();
        int[] array = new int[100];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000);
        }
        System.out.println(Arrays.toString(array));
        int sector = array.length / 3;

        ThreadSum threadSum1 = new ThreadSum(array, 0, sector);
        ThreadSum threadSum2 = new ThreadSum(array, sector, sector * 2);
        ThreadSum threadSum3 = new ThreadSum(array, sector * 2, array.length);

        threadSum1.start();
        threadSum2.start();
        threadSum3.start();

        try {
            threadSum1.join();
            threadSum2.join();
            threadSum3.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        int result = threadSum1.getSum() + threadSum2.getSum() + threadSum3.getSum();
        System.out.println("Result = " + result);
    }
}
