public class ThreadSum extends Thread {

    private int[] array;
    private int left;
    private int right;
    private int sum = 0;

    public ThreadSum(int[] array, int left, int right) {
        this.array = array;
        this.left = left;
        this.right = right;
    }

    @Override
    public void run() {
        for (int i = left; i < right; i++) {
            sum += this.array[i];
        }
        System.out.println("Sum of " + currentThread().getName() + " = " + sum);
    }

    public int getSum() {
        return sum;
    }
}
