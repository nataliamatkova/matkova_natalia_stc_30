//список на основе массива
public class ArrayList implements List {
    //константа для начального размера массива
    private static final int DEFAULT_SIZE = 10;
    //поле, представляющее собой массив, в котором храним элементы
    private int[] data;
    //количество элементов в массиве (count != length)
    private int count;

    public ArrayList() {
        this.data = new int[DEFAULT_SIZE];
    }

    private class ArrayListIterator implements Iterator {
        private int current = 0;

        @Override
        public int next() {
            int value = data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }


    @Override
    public int get(int index) {
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");
        return -1;
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(int element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1); // побитовый сдвиг вправо. oldLength >> 1 = oldLength/2
        int[] newData = new int[newLength];
        System.arraycopy(this.data, 0, newData, 0, oldLength);
        this.data = newData;
    }

    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(int element) {
        int indexOfRemovingElement = indexOf(element);
        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        count--;
    }

    @Override
    public void removeByIndex(int index) {
        if (index < count && index >= 0) {
            for (int i = index; i < count - 1; i++) { //проходим по массиву от index-а до конца
                this.data[i] = this.data[i + 1]; //и заменяем значения элементов на следующий
            }
            count--; //уменьшаем количество элементов в массиве
        } else {
            System.out.println("Такого индекса нет");
        }
    }

    @Override
    public void insert(int element, int index) {
        if (index <= count && index >= 0) {
            if (count == this.data.length - 1) {
                resize();
            } else {
                count++; //увеличиваем количество элементов в массиве
                for (int i = count - 1; i > index; i--) { //проходим массив в обратном порядке до index
                    this.data[i] = this.data[i - 1]; //и заменяем значения элементов на предыдущий
                }
                this.data[index] = element;
            }
        } else {
            System.out.println("Вышли за пределы массива");
        }
    }

    @Override
    public void reverse() {
    }
}
