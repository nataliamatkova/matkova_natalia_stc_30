public interface Iterator {
    // возвращает следующий элемент
    int next ();
    //проверяет, есть ли следующий эелемент
    boolean hasNext();
}
