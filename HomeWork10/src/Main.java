public class Main {
    public static void main(String[] args) {

        //ArrayList
        List arrayList = new ArrayList();

        for (int i = 0; i < 17; i++) {
            arrayList.add(i);
        }

        System.out.println("Size of array list: " + arrayList.size());
        System.out.println(arrayList.contains(7));
        arrayList.removeByIndex(9);
        arrayList.removeByIndex(-4);
        arrayList.insert(33, 5);
        System.out.println("Size of array list: " + arrayList.size());

        Iterator iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        //LinkedList
        List linkedList = new LinkedList();

        linkedList.add(9);
        linkedList.add(778);
        linkedList.add(23);
        linkedList.add(11);
        linkedList.add(56);
        linkedList.add(4);

        System.out.println("Size of linked list: " + linkedList.size());
        System.out.println(linkedList.get(1));
        linkedList.insert(19, 2);
        linkedList.insert(199, 12);
        System.out.println(linkedList.contains(56));
        System.out.println(linkedList.contains(100));
        linkedList.removeByIndex(4);
        linkedList.removeFirst(9);

        Iterator linkedListIterator = linkedList.iterator();
        while (linkedListIterator.hasNext()) {
            System.out.println(linkedListIterator.next());
        }

        linkedList.reverse();

        linkedListIterator = linkedList.iterator();
        while (linkedListIterator.hasNext()) {
            System.out.println(linkedListIterator.next());
        }
    }
}
