public class LinkedList implements List {

    private class LinkedListIterator implements Iterator {

        private Node element = first; // первый элемент
        private int elementCount = 0; //количество элементов

        @Override
        public int next() {
            int value = element.value; //значение элемента
            element = element.next; //ссылка на следующий элемент
            elementCount++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return elementCount < count;
        }
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }

    private Node first;
    private Node last;
    private int count;

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;
            while (i < index) {
                current = current.next;
                i++;
            }
            return current.value;
        }
        System.err.println("Такого элемента нет");
        return -1;
    }

    @Override
    public int indexOf(int element) {
        int i = 0;
        Node current = this.first;
        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }
        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
            // новый узел теперь последний
            last = newNode;
        }
        count++;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(int element) {
        if (this.contains(element)) {
            Node current = this.first;
            this.first = current.next;
            count--;
        } else {
            System.out.println("Такого элемента нет");
        }
    }

    @Override
    public boolean contains(int element) {
        return indexOf(element) != -1;
    }

    @Override
    public void insert(int element, int index) {
        Node newNode = new Node(element); // новый узел
        Node current = this.first; // текущий узел
        Node nextCurrent = current.next; // следующий после текущего
        if (index > 0 && index < count) {
            int i = 0;
            while (i < index - 1) { // проходим по всему списку до нужного индекса
                current = current.next;
                nextCurrent = current.next;
                i++;
            }
            current.next = newNode; // создаем связи для нового элемента
            newNode.next = nextCurrent;
            count++;
        } else if (index == 0) {
            newNode.next = this.first;
            this.first = newNode;
            count++;
        } else {
            System.out.println("Такого индекса нет");
        }
    }

    @Override
    public void removeByIndex(int index) {
        Node current = this.first; //текущий узел (index = 0)
        Node nextCurrent = current.next; // следующий после текущего (index = 1)
        if (index > 0 && index < count) {
            int i = 0;
            while (i < index - 1) { // проходим по всему списку до нужного индекса
                current = current.next; // текущему узлу присваивается значение следующего
                nextCurrent = current.next; // следущему узлу - следующий после текущего
                i++;
            }
            current.next = nextCurrent.next; //присваивается значение через элемент (пропускаем элемент, который нужно удалить)
            count--; // уменьшаем количество узлов
        } else if (index == 0) {
            this.removeFirst(index);
        } else {
            System.out.println("Такого индекса нет");
        }
    }

    @Override
    public void reverse() {

        /*int[] array = new int[count]; //вспомогательный массив такого же размера
        Node current = this.first;
        for (int i = count - 1; i >= 0; i--) { // заполняем массив элементами из списка в обратном порядке
            array[i] = current.value;
            current = current.next;
        }
        for (int i = 0; i < array.length; i++) { //заполняем список элементами из массива
            add(array[i]);*/

        Node current = this.first; // текущий узел, ссылается на первый элемент
        Node previous = null; // предыдущий узел, ссылка null
        while (current != null) { // проходим по списку
            Node next = current.next;  // следующий узел, ссылается на следующиий за текущим
            current.next = previous; // "разворачиваем" ссылку в обратную сторону, теперь следующий узел ссылается на предыдущий
            previous = current; // идем дальше по списку: от предыдущего узла к текущему
            current = next; // от текущему к следующему
        }
        this.first = previous;
    }
}
