import java.util.Arrays;
import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int opposite; // переменная, в которой будет хранится значение "противоположного" элемента

        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }

        for (int i = 0; i < array.length / 2; i++) {
            opposite = array[array.length - i - 1];
            array[array.length - i - 1] = array[i]; //замена элемента в массиве противоположным
            array[i] = opposite;
        }
        System.out.println(Arrays.toString(array));
    }
}