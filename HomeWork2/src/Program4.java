import java.util.Arrays;
import java.util.Scanner;

public class Program4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        int indexOfMin = 0;
        int indexOfMax = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[indexOfMin] > array[i]) {
                indexOfMin = i;
            } else if (array[indexOfMax] < array[i]) {
                indexOfMax = i;
            }
        }

        int temp = array[indexOfMin];
        array[indexOfMin] = array[indexOfMax];
        array[indexOfMax] = temp;
        System.out.println(Arrays.toString(array));
    }
}