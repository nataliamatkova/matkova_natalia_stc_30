import java.util.Arrays;
import java.util.Scanner;

public class Program5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length - 1; j++) {  //перебор значений
                if (array[j] > array[j + 1]) { //сравнение соседних элементов
                    int temp1 = array[j]; // временная переменная для замены значений
                    array[j] = array[j + 1];
                    array[j + 1] = temp1;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}