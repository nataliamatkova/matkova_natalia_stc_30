public class TV {
    private String tvName;
    private RemoteController rm;

    private Channel[] channels; //массив каналов
    private int channelsCount;

    public TV(String tvName) {
        /*this.tvName = tvName;
        this.channels = new Channel[3];*/
        this(tvName, 3);
    }
    public TV(String tvName, int channelsCount) {
        this.tvName = tvName;
        this.channels = new Channel[channelsCount];
    }

    public String getTvName() {
        return tvName;
    }

    public void addChannel(Channel channel) { //добавляет каналы на тв
        channels[channelsCount] = channel;
        channelsCount++;
    }

    public void selectChannelOnTV(int selectedChannel) {
        int k = 0;
        while (channels[k].getChannelNumber() != selectedChannel) {  //соответствие номера канала с выбранным каналом на пульте
            k++;
        }
        channels[k].RandomProgram();
    }
}

