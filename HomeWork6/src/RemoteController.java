import java.util.Scanner;

public class RemoteController {
    private TV tv;
    //private Channel channel;
    private int rmNumber;
    int selectedChannel;

    public RemoteController(int rmNumber) {
        this.rmNumber = rmNumber;
    }

    public void addTV(TV tv) { //добавляет пульт к телевизору
        this.tv = tv;
    }

    public int selectChannel() {  //выбор канала на пульте
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, select the channel number");
        selectedChannel = scanner.nextInt();
        return selectedChannel;
    }
}
