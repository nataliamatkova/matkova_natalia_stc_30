

public class Main {
    public static void main(String[] args) {
        TV tv = new TV("LG"); // телевизор
        RemoteController rm = new RemoteController(343); // пульт

        Channel channel1 = new Channel( "Perviy",1); //каналы
        Channel channel2 = new Channel("STS", 2);
        Channel channel3 = new Channel( "2x2",3);

        Program pr1 = new Program("News");  //программы
        Program pr2 = new Program("Good morning");
        Program pr3 = new Program("Weather time");
        Program pr4 = new Program("KVN");
        Program pr5 = new Program("The Lion King");
        Program pr6 = new Program("Home alone");
        Program pr7 = new Program("Avatar");
        Program pr8 = new Program("Simpsons");
        Program pr9 = new Program("Futurama");
        Program pr10 = new Program("South Park");

        channel1.addPrograms(pr1); //соответствие передач и каналов
        channel1.addPrograms(pr2);
        channel1.addPrograms(pr3);
        channel1.addPrograms(pr4);
        channel2.addPrograms(pr5);
        channel2.addPrograms(pr6);
        channel2.addPrograms(pr7);
        channel3.addPrograms(pr8);
        channel3.addPrograms(pr9);
        channel3.addPrograms(pr10);

        tv.addChannel(channel1); //соответствие каналов и телевизора
        tv.addChannel(channel2);
        tv.addChannel(channel3);

        rm.addTV(tv); //соответствие пульта и телевизора

        tv.selectChannelOnTV(rm.selectChannel()); //выбор канала
    }
}
