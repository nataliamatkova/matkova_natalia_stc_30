import java.util.Random;

public class Channel {
    private TV tv;
    private String channelName;
    private int channelNumber;

    private Program []programs; //массив программ
    private int programsCount;

    public Channel (String channelName, int channelNumber){
        this.channelName = channelName;
        this.channelNumber = channelNumber;
        this.programs = new Program[10];
    }

    public String getChannelName() {
        return channelName;
    }

    public int getChannelNumber() {
        return channelNumber;
    }

    public void addPrograms(Program program) {  //добавляет программы на каналы
        this.programs[programsCount] = program;
        this.programsCount++;
    }

    public void RandomProgram(){  //случайный выбор программы
        Random random = new Random();
        int i = random.nextInt(programsCount);
        System.out.println(programs[i].getProgramName() + " - " + channelName + " channel");
    }
}
