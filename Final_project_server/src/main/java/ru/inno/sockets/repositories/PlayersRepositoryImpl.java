package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;

import javax.sql.DataSource;
import java.sql.*;

public class PlayersRepositoryImpl implements PlayersRepository {
    private DataSource dataSource;


    private static final String SQL_INSERT_PLAYER = "insert into player (player_nickname, ip) values (?, ?);";
    private static final String SQL_FIND_BY_NICKNAME = "select * from player where player_nickname = ?;";
    private static final String SQL_FIND_PLAYER_BY_ID = "select * from player where player_id = ?";
    private static final String SQL_UPDATE_PLAYER = "update player set player_nickname = ?, ip = ?, count_losings = ?," +
                                                     " count_wins = ?, score = ? where player_nickname = ?;";
    private static final String SQL_UPDATE_IP = "update player set  ip = ? where player_nickname = ?;";

    public PlayersRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Player findByNickname(String nickname) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            preparedStatement.setString(1, nickname);
            ResultSet result = preparedStatement.executeQuery();
            if (result.next()) {
                return Player.builder()
                        .playerId(result.getLong("player_id"))
                        .nickname(result.getString("player_nickname"))
                        .ip(result.getString("ip"))
                        .countLosings(result.getInt("count_losings"))
                        .countWins(result.getInt("count_wins"))
                        .score(result.getInt("score"))
                        .build();
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Player findById(Long playerId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_ID)) {
            statement.setLong(1, playerId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                return Player.builder()
                        .playerId(result.getLong("player_id"))
                        .nickname(result.getString("player_nickname"))
                        .ip(result.getString("ip"))
                        .countLosings(result.getInt("count_losings"))
                        .countWins(result.getInt("count_wins"))
                        .score(result.getInt("score"))
                        .build();
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {

        try (Connection connection = dataSource.getConnection();
             // при создании объекта, который может посылать запросы в бд, указываю Statement.RETURN_GENERATED_KEYS
             // -> это значит, что запрос должен вернуть сгенерированные ключи из бд обратно в Java
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS);) {
            statement.setString(1, player.getNickname());
            statement.setString(2, player.getIp());
            // выполнили запрос, и получили количество строк, которое мы вставили, оно не должно быть нулевым
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save player");
            }
            // generatedIds - множество строк, которое содержит сгенерированные ключи после выполнения запроса
            ResultSet generatedIds = statement.getGeneratedKeys();
            // next() возвращает true, если в generatedIds что-то есть, если там ничего нет - значит что-то пошло не такх
            if (generatedIds.next()) {
                // из generatedIds получаем сгенерированный ключ с названием id
                int generatedId = generatedIds.getInt("player_id");
                // для сохраняемой сущности проставляем id, который получили для этой сущности из базы
                player.setPlayerId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER)) {
            statement.setString(1, player.getNickname());
            statement.setString(2, player.getIp());
            statement.setInt(3, player.getCountLosings());
            statement.setInt(4, player.getCountWins());
            statement.setInt(5, player.getScore());
            statement.setString(6, player.getNickname());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update player");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void updateIp(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_IP)) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getNickname());
            int insertedRowsCount = statement.executeUpdate();
            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update player");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public boolean findPlayerByNickname(String nickname) {
        boolean playerIsFound = false;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            statement.setString(1, nickname);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                if (nickname.equals(result.getString("player_nickname"))) {
                    playerIsFound = true;
                } else {
                    System.out.println("Player is not found");
                }
            }
            result.close();
            return playerIsFound;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }


}

