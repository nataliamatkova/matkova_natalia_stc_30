package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;

public interface GamesRepository {

    void save(Game game);
    void update(Game game);
    Game findById(Long gameId);

}
