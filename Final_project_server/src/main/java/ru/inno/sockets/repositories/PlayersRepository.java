package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Player;


public interface PlayersRepository {

    Player findByNickname(String nickname);
    Player findById(Long playerId);
    void save(Player player);
    void update(Player player);
    void updateIp(Player player);
    boolean findPlayerByNickname(String nickname);


}
