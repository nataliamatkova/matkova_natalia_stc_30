package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryImpl implements ShotsRepository {

    private DataSource dataSource;

    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private static final String SQL_INSERT_SHOT = "insert into shot (shooter_player_nickname, target_player_nickname, game_id, shot_time ) " +
                                                   " values (?, ?, ?, ?);";

    public void save(Shot shot) {
        System.out.println("Выстрел сохранен, " + shot.getShooter().getNickname() + " стреляет в "
                + shot.getTarget().getNickname() + " в игре: " + shot.getGame().getGameId());

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, shot.getShooter().getNickname());
            statement.setString(2, shot.getTarget().getNickname());
            statement.setLong(3, shot.getGame().getGameId());
            statement.setString(4, shot.getShotTime());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save shot");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("shot_id");
                shot.setShotId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}

