package ru.inno.sockets.repositories;

import ru.inno.sockets.models.Game;
import javax.sql.DataSource;
import java.sql.*;

public class GamesRepositoryImpl implements GamesRepository {

    private static final String SQL_INSERT_GAME = "insert into game (first_player_nickname, second_player_nickname, game_date, duration)" +
                                                  " values (?, ?, ?, ?);";
    private static final String SQL_FIND_GAME_BY_ID = "select * from game where game_id = ?";
    private static final String SQL_UPDATE_GAME = "update game set first_player_shots_count = ?, second_player_shots_count = ?, duration = ? " +
                                                  " where game_id = ?;";

    private DataSource dataSource;

    public GamesRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        System.out.println("Игра началась для игроков: " + game.getFirstPlayer().getNickname() + ", "
                + game.getSecondPlayer().getNickname() + " в игре: " + game.getGameId());
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getFirstPlayer().getNickname());
            statement.setString(2, game.getSecondPlayer().getNickname());
            statement.setString(3, game.getGameDate());
            statement.setLong(4, game.getDuration());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot save game");
            }
            ResultSet generatedIds = statement.getGeneratedKeys();
            if (generatedIds.next()) {
                int generatedId = generatedIds.getInt("game_id");
                game.setGameId((long) generatedId);
            } else {
                throw new SQLException("Cannot return id");
            }
            generatedIds.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Long gameId) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID)) {
            statement.setLong(1, gameId);
            ResultSet result = statement.executeQuery();

            if (result.next()) {
                PlayersRepository playersRepository = new PlayersRepositoryImpl(dataSource);
                return Game.builder()
                        .gameId(result.getLong("game_id"))
                        .gameDate(result.getString("game_date"))
                        .firstPlayer(playersRepository.findByNickname(result.getString("first_player_nickname")))
                        .secondPlayer(playersRepository.findByNickname(result.getString("second_player_nickname")))
                        .firstPlayerShotsCount(result.getInt("first_player_shots_count"))
                        .secondPlayerShotsCount(result.getInt("second_player_shots_count"))
                        .duration(result.getLong("duration"))
                        .build();
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setInt(1, game.getFirstPlayerShotsCount());
            statement.setInt(2, game.getSecondPlayerShotsCount());
            statement.setLong(3, game.getDuration());
            statement.setLong(4, game.getGameId());
            int insertedRowsCount = statement.executeUpdate();

            if (insertedRowsCount == 0) {
                throw new SQLException("Cannot update game");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}

