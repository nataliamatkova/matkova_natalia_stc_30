package ru.inno.sockets.services;

import ru.inno.sockets.models.Player;

public interface TanksService {

    Player createOrUpdatePlayerInDataBase(Player player);
    Long startGame(String firstPlayerNickname, String secondPlayerNickname);
    void shot(Long gameId, String shooterNickname, String targetNickname);
    void finishGame(Long gameId, String winnerNickname, String loserNickname);


}
