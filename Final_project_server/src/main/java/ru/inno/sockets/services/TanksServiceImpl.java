package ru.inno.sockets.services;

import ru.inno.sockets.models.Game;
import ru.inno.sockets.models.Player;
import ru.inno.sockets.models.Shot;
import ru.inno.sockets.repositories.*;
import ru.inno.sockets.server.GameServer;

import java.time.LocalTime;
import java.time.LocalDate;

public class TanksServiceImpl implements TanksService {

    private ShotsRepository shotsRepository;
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;

    public TanksServiceImpl(ShotsRepository shotsRepository, PlayersRepository playersRepository, GamesRepository gamesRepository) {
        this.shotsRepository = shotsRepository;
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
    }

    @Override
    public Long startGame(String firstPlayerNickname, String secondPlayerNickname) {

        Player firstPlayer = playersRepository.findByNickname(firstPlayerNickname);
        Player secondPlayer = playersRepository.findByNickname(secondPlayerNickname);
        firstPlayer = new Player(firstPlayerNickname);
        secondPlayer = new Player(secondPlayerNickname);

        // устанавливаем ip для игроков
        firstPlayer.setIp(GameServer.ipAddress.get(0));
        secondPlayer.setIp(GameServer.ipAddress.get(1));

        // добавляем игрока в БД или обновляем данные по нему ip, если он уже есть в БД
        createOrUpdatePlayerInDataBase(firstPlayer);
        createOrUpdatePlayerInDataBase(secondPlayer);

        Game game = new Game();
        game.setFirstPlayer(firstPlayer);
        game.setSecondPlayer(secondPlayer);
        game.setGameDate(LocalDate.now().toString());
        // засекаем время
        Long startTime = System.currentTimeMillis();
        game.setDuration(startTime);

        gamesRepository.save(game);
        return game.getGameId();
    }

    @Override
    public Player createOrUpdatePlayerInDataBase(Player player) {
        // если игрок с таким ником найден, значит нужно обновить его Ip
        if (playersRepository.findPlayerByNickname(player.getNickname())) {
            playersRepository.updateIp(player);
        } else { // если не найден, добавляем нового игрока в базу данных
            playersRepository.save(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(shooter, target, game);
        if (game.getFirstPlayer().equals(shooter)) {
            game.setFirstPlayerShotsCount(game.getFirstPlayerShotsCount() + 1);
        } else if (game.getSecondPlayer().equals(shooter)) {
            game.setSecondPlayerShotsCount(game.getSecondPlayerShotsCount() + 1);
        }
        shot.setShotTime(LocalTime.now().toString());
        shotsRepository.save(shot);
        gamesRepository.update(game);
        System.out.println(" shots: " +
                game.getFirstPlayerShotsCount() + "|" + game.getSecondPlayerShotsCount());
    }

    @Override
    public void finishGame(Long gameId, String winnerNickname, String loserNickname) {
        Game game = gamesRepository.findById(gameId);
        Player winner = playersRepository.findByNickname(winnerNickname);
        Player loser = playersRepository.findByNickname(loserNickname);

        if (winner.getNickname().equals(game.getFirstPlayer().getNickname())) { // если победил первый игрок
            // увеличиваем ему количество очков на сумму выстрелов в игре
            winner.setScore(game.getFirstPlayer().getScore() + game.getFirstPlayerShotsCount());
            // второму игроку уменьшаем количество очков на сумму выстрелов в игре
            loser.setScore(game.getSecondPlayer().getScore() - game.getSecondPlayerShotsCount());
        } else if ((winner.getNickname().equals(game.getSecondPlayer().getNickname()))) { // если победил второй игрок
            winner.setScore(game.getSecondPlayer().getScore() + game.getSecondPlayerShotsCount());
            loser.setScore(game.getFirstPlayer().getScore() - game.getFirstPlayerShotsCount());
        }
        // считаем разницу во времени начала и окончания игры в секундах
        Long endTime = System.currentTimeMillis();
        game.setDuration((endTime - game.getDuration()) / 1000);
        gamesRepository.update(game);

        //увеличиваем счетчик побед/поражений
        winner.setCountWins(winner.getCountWins() + 1);
        loser.setCountLosings(loser.getCountLosings() + 1);

        playersRepository.update(winner);
        playersRepository.update(loser);

    }
}
