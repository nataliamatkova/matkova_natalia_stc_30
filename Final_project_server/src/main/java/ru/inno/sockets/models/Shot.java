package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder

public class Shot {
    private Long shotId;
    private Player shooter;
    private Player target;
    private Game game;
    private String shotTime;

    public Shot() {
    }

    public Shot(Player shooter, Player target, Game game) {
        this.shooter = shooter;
        this.target = target;
        this.game = game;
    }
}