package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor

public class Player {
    private Long playerId;
    private String nickname;
    private String ip;
    private Integer countLosings = 0;
    private Integer countWins = 0;
    private Integer score = 0;

    public Player() {
    }

    public Player(String nickname) {
        this.nickname = nickname;
    }

}

