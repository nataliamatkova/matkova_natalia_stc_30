package ru.inno.sockets.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder

public class Game {
    private Long gameId;
    private String gameDate;
    private Player firstPlayer;
    private Player secondPlayer;
    private Integer firstPlayerShotsCount = 0;
    private Integer secondPlayerShotsCount = 0;
    private Long duration = 0L;

    public Game() {
    }
}

