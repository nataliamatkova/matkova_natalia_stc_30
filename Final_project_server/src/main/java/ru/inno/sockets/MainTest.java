package ru.inno.sockets;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.sockets.models.Player;
import ru.inno.sockets.repositories.GamesRepository;
import ru.inno.sockets.repositories.GamesRepositoryImpl;
import ru.inno.sockets.repositories.PlayersRepository;
import ru.inno.sockets.repositories.PlayersRepositoryImpl;

public class MainTest {
    public static void main(String[] args) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/stc30_final_project");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("qwerty2020");
        hikariConfig.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);
        PlayersRepository playersRepository = new PlayersRepositoryImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryImpl(dataSource);
        //System.out.println(playersRepository.findPlayerByNickname("jack"));
        //System.out.println(playersRepository.findPlayerByNickname("met"));
        //playersRepository.findNick("met");
        //Player player1 = new Player("john");
        //playersRepository.update(player1);
        //System.out.println(playersRepository.findById(10L));
       // System.out.println(playersRepository.findByNickname("jack"));
        System.out.println(gamesRepository.findById(9L));

    }
}
