import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] numbers = {3, 234, 56, 12, 8, 405, 7012, 3303, -456, 500, 1001, 22, 44404};
        String[] strings = {"hello1", "2bye", "Good Morn33ing"};
        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor(numbers, strings);


        //разворачиваем исходные числа
        /*решение с помощью анонимного класса
        NumbersProcess reverseNumbers = new NumbersProcess() {
            @Override
            public int process(int number) {
                int result = 0;
                int copyNumber = number;
                while (copyNumber != 0) {
                    result = copyNumber % 10 + result * 10;
                    copyNumber /= 10;
                }
                return result;
            }
        };*/
        NumbersProcess reverseNumbers = number -> {
            int result = 0;
            while (number != 0) {
                result = number % 10 + result * 10;
                number /= 10;
            }
            return result;
        };

        //удаляем нули из чисел
        NumbersProcess removeZero = number -> {
            int result = 0;
            int n = 1; //разряд числа - "место", которая цифра занимает в числе
            while (number != 0) {
                result += number % 10 * n;
                if (number % 10 != 0) {
                    n *= 10;
                }
                number /= 10;
            }
            return result;
        };

        //заменяем четные цифры ближайшей нечетной снизу (7 на 6)
        NumbersProcess replaceOddToEven = number -> {
            int result = 0;
            int n = 1; //разряд числа - "место", которая цифра занимает в числе
            while (number != 0) {
                if ((number % 10) % 2 == 0) {
                    result += number % 10 * n;
                } else {
                    result += ((number % 10) - 1) * n;
                }
                n *= 10;
                number /= 10;
            }
            return result;
        };

        //разворачиваем исходные строки
        StringsProcess reverseString = string -> {
            String result = "";
            //переводим строку в массив array с помощью метода toCharArray
            char[] array = string.toCharArray();
            //проходим по всему массиву в обратном порядке
            for (int i = array.length - 1; i >= 0; i--) {
                result += array[i];
            }
            return result;
        };

        //удаляем все цифры из строк с помощью метода replaceAll
        StringsProcess removeDigits = string -> string.replaceAll("[0-9]", "");

        //заменяем все маленькие буквы большими с помощью метода toUpperCase
        StringsProcess toUpperCase = string -> string.toUpperCase();


        System.out.println("Source numbers: " + Arrays.toString(numbers));
        System.out.println("Reversed numbers: " + Arrays.toString(numbersAndStringProcessor.process(reverseNumbers)));
        System.out.println("Numbers without zero: " + Arrays.toString(numbersAndStringProcessor.process(removeZero)));
        System.out.println("Replace odd number: " + Arrays.toString(numbersAndStringProcessor.process(replaceOddToEven)));

        System.out.println("Source strings: " + Arrays.toString(strings));
        System.out.println("Reversed strings: " + Arrays.toString(numbersAndStringProcessor.process(reverseString)));
        System.out.println("Strings without digits: " + Arrays.toString(numbersAndStringProcessor.process(removeDigits)));
        System.out.println("To upper case:  " + Arrays.toString(numbersAndStringProcessor.process(toUpperCase)));

    }
}
