//класс принимает на вход массив строк и массив чисел

public class NumbersAndStringProcessor {

    private int[] numberSequence;
    private String[] stringSequence;

    public NumbersAndStringProcessor(int[] numberSequence, String[] stringSequence) {
        this.numberSequence = numberSequence;
        this.stringSequence = stringSequence;
    }

    public int[] process(NumbersProcess process) {
        //создаем новый массив чисел, размер которого = исходному
        int[] result = new int[numberSequence.length];
        for (int i = 0; i < numberSequence.length; i++) {
            //копируем элементы исходного массива в новый
            result[i] = process.process(numberSequence[i]);
        }
        return result;
    }

    public String[] process(StringsProcess process) {
        //создаем новый массив строк, размер которого = исходному
        String[] result = new String[stringSequence.length];
        for (int i = 0; i < stringSequence.length; i++) {
            //копируем элементы исходного массива в новый
            result[i] = process.process(stringSequence[i]);
        }
        return result;
    }
}
