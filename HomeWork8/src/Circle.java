import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Circle extends GeometricFigure {
    public Circle(double firstSide, double x, double y, String name) {
        super(firstSide, x, y, name);
    }

    @Override
    public void calculateArea() {
        System.out.println("Area of " + this.name + " = " + pow(this.firstSide, 2) * PI);
    }

    @Override
    public void calculatePerimeter() {
        System.out.println("Perimeter of " + this.name + " = " + this.firstSide * 2 * PI);
    }

}
