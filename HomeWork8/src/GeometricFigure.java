public abstract class GeometricFigure implements Scalable, Relocatable {
    protected double firstSide; //длина одной стороны фигуры
    protected double x; // координата центра по оси х
    protected double y; // координата центра по оси у
    protected String name;

    public GeometricFigure(double firstSide, double x, double y, String name) {
        this.firstSide = firstSide;
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public abstract void calculateArea(); //рассчет площади

    public abstract void calculatePerimeter(); //рассчет периметра

    @Override
    public void relocate(double valueX, double valueY) {  //меняет координаты центра фигуры
        this.x += valueX;
        this.y += valueY;
        System.out.println("New coordinates for " + this.name + ": " + this.x + ", " + this.y);
    }

    @Override
    public void changeScale(double value) {  //увеличивает или уменьшает размер фигуры на заданное значение
        if ((this.firstSide += value) <= 0) { //проверка, если длина стороны получилась отрицательной
            System.out.println("Invalid value");
        } else
            System.out.println("New size of " + this.name + ": " + this.firstSide);
    }

}
