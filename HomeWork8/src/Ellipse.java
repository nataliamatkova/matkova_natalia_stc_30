import static java.lang.Math.PI;
import static java.lang.Math.pow;

public class Ellipse extends Circle {
    private double secondSide; //длина второй стороны

    public Ellipse(double firstSide, double x, double y, String name, double secondSide) {
        super(firstSide, x, y, name);
        this.secondSide = secondSide;
    }

    @Override
    public void calculateArea() {
        System.out.println("Area of " + this.name + " = " + this.firstSide * this.secondSide * PI);
    }

    @Override
    public void calculatePerimeter() {
        System.out.println("Perimeter of " + this.name + " = " + 4 * (this.firstSide * this.secondSide * PI + pow((this.firstSide - this.secondSide), 2)) / (this.firstSide + this.secondSide));
    }

    @Override
    public void changeScale(double value) {
        if ((this.firstSide += value) <= 0 || (this.secondSide += value) <= 0) { //проверка, если длина одной из сторон получилась отрицательной
            System.out.println("Invalid value");
        } else
            System.out.println("New size of " + this.name + ": " + this.firstSide + ", " + this.secondSide);
    }


}
