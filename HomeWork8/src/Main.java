public class Main {
    public static void main(String[] args) {

        Circle circle = new Circle(5, 0, 0, "circle");
        Rectangle rectangle = new Rectangle(5, 4.5, 0, "rectangle", 2);
        Square square = new Square(8.5, 0, 0, "square");
        Ellipse ellipse = new Ellipse(10, 0, 0, "ellipse", 6.7);

        circle.calculateArea();
        circle.calculatePerimeter();
        rectangle.calculateArea();
        rectangle.calculatePerimeter();
        square.calculateArea();
        square.calculatePerimeter();
        ellipse.calculateArea();
        ellipse.calculatePerimeter();

        circle.changeScale(2); //увеличение
        rectangle.changeScale(1); //увеличение
        square.changeScale(-1); //уменьшение
        square.changeScale(-7.5); //недопустимое значение
        ellipse.changeScale(-7); //недопустимое значение

        circle.relocate(1, 2);
        rectangle.relocate(3.5, -1);
        square.relocate(0, 4);
        ellipse.relocate(-2, 0);


    }
}
