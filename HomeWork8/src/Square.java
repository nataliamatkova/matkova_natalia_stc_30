public class Square extends GeometricFigure {
    public Square(double firstSide, double x, double y, String name) {
        super(firstSide, x, y, name);
    }

    @Override
    public void calculateArea() {
        System.out.println("Area of " + this.name + " = " + this.firstSide * this.firstSide);
    }

    @Override
    public void calculatePerimeter() {
        System.out.println("Perimeter of " + this.name + " = " + this.firstSide * 4);
    }

}
