public class Main {
    public static void main(String[] args) {

        User user1 = new User.Builder()
                .firstName("Ivan")
                .lastName("Petrov")
                .age(25)
                .isWorker(true)
                .builder();

        User user2 = new User.Builder()
                .firstName("Alexander")
                .lastName("Somov")
                /*.age(26)
                .isWorker(true)*/
                .builder();

        user1.printInfo();
        user2.printInfo();
    }
}
