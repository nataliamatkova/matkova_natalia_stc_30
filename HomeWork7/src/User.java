public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    public static class Builder { //статич. вложенный класс
        private User newUser;


        public Builder() {  //конструктор
            newUser = new User();
        }

        public Builder firstName(String firstName) { //параметры
            newUser.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            newUser.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            newUser.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            newUser.isWorker = isWorker;
            return this;
        }

        public User builder() { //метод возвращает готовый объект
            return newUser;
        }
    }



    public void printInfo() { //выводит информацию о пользователе
        System.out.println("First name: " + firstName);
        System.out.println("Last name: " + lastName);
        System.out.println("Age: " + age);
        System.out.println("Is worker: "+ isWorker);
    }
}
