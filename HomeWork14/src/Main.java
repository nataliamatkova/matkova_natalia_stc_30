import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {
    public static void main(String[] args) {
        try {
            // Вывести номера всех автомобилей, имеющих черный цвет или нулевой пробег
            Files.lines(Paths.get("Cars.txt"))
                    .map(line -> line.split(", "))
                    .map(array -> new Cars(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .filter(car -> car.getColor().equals("black") || car.getMileage() == 0)
                    .forEach(car -> System.out.println(car.getNumber()));

            // Вывести количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
            Long count = Files.lines(Paths.get("Cars.txt"))
                    .map(line -> line.split(", "))
                    .map(array -> new Cars(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .filter(car -> car.getPrice() >= 700000 && car.getPrice() <= 800000)
                    .distinct()
                    .count();
            System.out.println(count);

            // Вывести цвет автомобиля с минимальной стоимостью
            String color = Files.lines(Paths.get("Cars.txt"))
                    .map(line -> line.split(", "))
                    .map(array -> new Cars(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .min((x, y) -> x.getPrice() - y.getPrice()).get()
                    .getColor();
            System.out.println(color);

            // Вывести среднюю стоимость Camry
            double average = Files.lines(Paths.get("Cars.txt"))
                    .map(line -> line.split(", "))
                    .map(array -> new Cars(array[0], array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4])))
                    .filter(car -> car.getModel().equals("Toyota Camry"))
                    .mapToDouble(car -> car.getPrice()).average().getAsDouble();
            System.out.println(average);

        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
