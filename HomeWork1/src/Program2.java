public class Program2 {
    public static void main(String args[]) {
        int number = 12345;
        int binaryNumber1 = number%2;
        int binaryNumber2 = number/2%2;
        int binaryNumber3 = number/4%2;
        int binaryNumber4 = number/8%2;
        int binaryNumber5 = number/16%2;
        int binaryNumber6 = number/32%2;
        int binaryNumber7 = number/64%2;
        int binaryNumber8 = number/128%2;
        int binaryNumber9 = number/256%2;
        int binaryNumber10 = number/512%2;
        int binaryNumber11 = number/1024%2;
        int binaryNumber12 = number/2048%2;
        int binaryNumber13 = number/4096%2;
        int binaryNumber14 = number/8192%2;
        System.out.println(binaryNumber14+""+binaryNumber13+""+binaryNumber12+""+binaryNumber11+""+binaryNumber10+
                ""+binaryNumber9+""+binaryNumber8+""+binaryNumber7+""+binaryNumber6+""+binaryNumber5+""+binaryNumber4+
                ""+binaryNumber3+""+binaryNumber2+""+binaryNumber1);
    }
}

